import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { IonSegment, IonSegmentButton } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { NoticiasService } from 'src/app/services/noticias.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  noticias: Article[] = [];
  categorias:string[] = [
    "business",
    "entertainment",
    "general",
    "healths",
    "ciences",
    "ports",
    "technology"
  ]
  constructor( private noticiasService: NoticiasService ) {}
  @ViewChild(IonSegment) segment: IonSegment;

  ngOnInit(){
    
  }

  ngAfterViewInit() {
    //aqui es que funciona el ViewChild(IonSegment)
    this.cargarNoticias( this.categorias[0] )
    console.log(this.segment,"ngAfterViewInit")
    if (this.segment) {
      
      this.segment.value = this.categorias[0]
      console.log(this.segment.value)
    }
  };


  cambioCategoria( event ){
    console.log(event)
    this.noticias = []
    this.cargarNoticias( event.detail.value )
  }

  cargarNoticias( categoria:string, event? ){
    this.noticiasService.getTopHeadlineCategoria( categoria )
        .subscribe( resp => 
                    {
                      this.noticias.push(...resp.articles)
                      console.log(resp ,"respuesta de las categorias!!") 
                      if ( event ) {
                        event.target.complete();
                      }
                    }
                  )
  }

  loadData( infiniteScroll ){
    this.cargarNoticias( this.segment.value, infiniteScroll );
  }

}
