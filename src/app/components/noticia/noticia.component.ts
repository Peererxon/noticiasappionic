import { Component, Input, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActionSheetController } from '@ionic/angular';
import { Article } from 'src/app/interfaces/interfaces';
import { DataLocalService } from 'src/app/services/data-local.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {
  @Input() noticia:Article
  @Input('id') indice:number = 0 
  @Input() enFavoritos;
  //recibiendo como id y renombrando como indice
  constructor(
              private iab: InAppBrowser,
              private actionSheetController: ActionSheetController,
              private socialSharing: SocialSharing,
              private dataLocalService:DataLocalService
              ) { }

  ngOnInit() {}

  abrirNoticia(){
    const browser = this.iab.create(this.noticia.url, '_system');
  }

  async lanzarMenu(){
    let guaradarBorrarBoton;

    if( this.enFavoritos )
    {
      guaradarBorrarBoton = {
        text: 'Borrar Favorito',
        icon: 'trash',
        cssClass: "action-dark",
        handler: () => {
          this.dataLocalService.borrarNoticia( this.noticia )
          console.log('Borrar de favorito');
        }
      }
    }else
    {
      guaradarBorrarBoton = {
        text: 'Favorito',
        icon: 'star',
        cssClass: "action-dark",
        handler: () => {
          this.dataLocalService.guardarNoticia(this.noticia)
          console.log('Favorito');
        }
      }
    }
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Compartir',
        icon: 'share',
        cssClass: "action-dark",
        handler: () => {
          //le permite al usuario decidir en donde lo va a compartir
          this.socialSharing.share(
            this.noticia.title,
            this.noticia.source.name,
            null,
            this.noticia.url
          )
          console.log('Share clicked');
        }
      },
      guaradarBorrarBoton,
      {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        cssClass: "action-dark",
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    })
    await actionSheet.present();

  }
}
