import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToHeadlineResponse } from '../interfaces/interfaces';


const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-key': apiKey
})
@Injectable({
  providedIn: 'root'
})

export class NoticiasService {
  headlinePagesNumber = 0;
  
  categoriaActual ="";
  categoriaPage = 0;
  constructor(private http: HttpClient) { }
  private ejecutarQuery<T>( query:string ){
    query = apiUrl + query
    return this.http.get<T>( query, { headers } )
  }

  getTopHeadLines(){
    this.headlinePagesNumber ++;
    return this.ejecutarQuery<ToHeadlineResponse>( "/top-headlines?country=us&page="+this.headlinePagesNumber  )
    //return this.http.get<ToHeadlineResponse>('http://newsapi.org/v2/top-headlines?country=us&apiKey=879b52e942f444e6bc2a22219471b9c4')
  }

  getTopHeadlineCategoria( categoria:string ){
//    console.log( "/top-headlines?country=us&category="+categoria+"&page="+this.categoriaPage)
    if( this.categoriaActual === categoria ){
      this.categoriaPage++
    }else{
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }
    return this.ejecutarQuery<ToHeadlineResponse>( "/top-headlines?country=us&category="+categoria+"&page="+this.categoriaPage)
    //return this.http.get("http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=879b52e942f444e6bc2a22219471b9c4")
  }
}
