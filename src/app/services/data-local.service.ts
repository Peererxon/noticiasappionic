import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ToastController } from '@ionic/angular';
import { Article } from '../interfaces/interfaces';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  noticias: Article[] = [];
  constructor( private toastController:ToastController) { 
    this.cargarFavoritos()
  }

  async guardarNoticia( noticia:Article ){
    const existe = this.noticias.find( noticiaArr => noticiaArr.title === noticia.title )
    if( !existe ){
      this.noticias.unshift(noticia)
      //console.log("no existia antes");
      
      Storage.set(
        { 
          key:'favoritos',
          value:  JSON.stringify(this.noticias) 
          
        }
      )
      this.presentToast('Agregado a favoritos');
      const favoritoAgregado = await Storage.get( { key:'favoritos' } )
      //console.log( favoritoAgregado ) 
    }
  }

  async cargarFavoritos(  ){
    const favoritos = await Storage.get({key:'favoritos'})
    console.log('favoritos ', favoritos)
    if ( favoritos.value )
    {
      this.noticias = JSON.parse(favoritos.value);
      //without this set noticias to null when no favorites was charged
    }
    
  }

  borrarNoticia( noticia:Article ){
    this.noticias = this.noticias.filter( noti=> noti.title !== noticia.title )
    Storage.set({ key:'favoritos', value: JSON.stringify(this.noticias) } )
    //actualizando las noticias con el nuevo arreglo

    this.presentToast('Borrado de favoritos');
  }

  async presentToast( message: string ) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
